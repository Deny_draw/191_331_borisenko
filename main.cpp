#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "webappcontroller.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;                                    // создание объекта движка/интерпретатора QML
    const QUrl url(QStringLiteral("qrc:/main.qml"));                 // задание файла QML-разметки для стартовой страницы окна приложения

    // конструкция ниже задает связь между событием "objectCreated" объекта "engine"
    // и колбеком
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app,
                     [url](QObject *obj, const QUrl &objUrl)         // колбек, лямбда выражение, т.е. безымянная функция, объявленная
                    {
                        if (!obj && url == objUrl)
                            QCoreApplication::exit(-1);              // действия на случай ошибки внутри движка
                    }, Qt::QueuedConnection);

    engine.load(url);                                                // загрузить файл стартовой страницы в движок

    QObject *rootItem = engine.rootObjects()[0];
    WebAppController httpController(rootItem);
    QObject::connect(rootItem, SIGNAL(btnHttpRequest()),
            &httpController, SLOT(getPageInfo())); // связь сигнала из qml при нажатии на кнопку и слота класса HTTPController
    return app.exec();                                               // начало работы приложения, т.е. передача управления
                                                                     // от точки входа коду самого приложения (cpp и qml)
}

/** строение:
*.pro - файл настроек системы сборки qmake
*   - все файлы из дерева
*       проекта перечислены в *.pro, и при удалении из *.pro-файла удаляются из дерева
*   - внешние библиотеки (.lib и .h-файлы) подключаются через *.pro
*   - различие процесса сборки для ОС задается в *.pro
*
* main.cpp - точка входа в приложение. А в случае приложения QML в main создается
* объект движка-интерпретатора QML-разметки
*
* Как и в любом C++, в проекте могут быть и другие .cpp и .h файлы
*
* Описание интерфейса приложения и простейших механик его логики содержится в
* файлах QML, которые выполняют роль фронтенда. QML - диалект JS + JSON
*
* .qrc
**/
