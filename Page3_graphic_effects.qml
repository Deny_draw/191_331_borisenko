import QtQuick 2.14
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.14
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.3
import QtQuick.Controls.Material 2.12
//import Qt.labs.platform 1.1

Page {
    id: page3_graphic_effects

    // Оригинальный дизайн: https://sun9-35.userapi.com/impf/PxzmeOG0c4iSgFd74mnWjqtmably__rm4kc03g/R3BsSesZSPg.jpg?size=337x600&quality=96&sign=0d68622965c25f07cc9ddc10d960fa7e&type=album
    header: Rectangle {
        id: page_header
        color: "#020202"
        y: 0; height: 50

        Label {
            text: "Графические эффекты"
            leftPadding: 10
            // для выравнивания текста по центру по левой стороне
            anchors.verticalCenter: page_header.verticalCenter
            anchors.left: page_header.left
        }

        Image {
            id: overflow_vertical_img
            source: "overflow_vertical.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: page_header.right
        }

        Image {
            id: edit_img
            source: "edit.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: overflow_vertical_img.left
            anchors.rightMargin: 10
        }

        Image {
            id: search_img
            source: "search.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: edit_img.left
            anchors.rightMargin: 10
        }

        Image {
            id: reload_img
            source: "page_reload.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: search_img.left
            anchors.rightMargin: 10
        }
    }

    ColumnLayout{
        id: effects_page_body
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.fill: parent

        TabBar {
            id: effects_pages
            width: parent.width
            anchors.top: parent.top
            Material.accent: "blue"
            anchors.fill: effects_page_body

            TabButton {
                text: qsTr("ThresholdMask")
                Material.accent: "#3ecaff"
                onClicked: {
                    thresholdmask.visible = true
                    opacitymask.visible = false
                    brightnesscontrast.visible = false
                    image_body1.visible = true
                    image_body2.visible = false
                    image_body3.visible = false
                }
            }

            TabButton {
                text: qsTr("OpacityMask")
                Material.accent: "#3ecaff"
                onClicked: {
                    thresholdmask.visible = false
                    opacitymask.visible = true
                    brightnesscontrast.visible = false
                    image_body1.visible = false
                    image_body2.visible = true
                    image_body3.visible = false
                }
            }

            TabButton {
                text: qsTr("BrightnessContrast")
                Material.accent: "#3ecaff"
                onClicked: {
                    thresholdmask.visible = false
                    opacitymask.visible = false
                    brightnesscontrast.visible = true
                    image_body1.visible = false
                    image_body2.visible = false
                    image_body3.visible = true
                }
            }
        }

        ColumnLayout{
            width: effects_pages.width - 20
            anchors.horizontalCenter: effects_pages.horizontalCenter

            ColumnLayout{
                id: image_body1
                anchors.top: effects_pages.bottom
                anchors.horizontalCenter: effects_page_body.horizontalCenter
                visible: true

                Image {
                     id: coffee1
                     source: "coffee.jpg"
                     width: image_body1.width
                     height: image_body1.height
                     fillMode: Image.PreserveAspectFit
                     smooth: true
                     visible: false
                }

                Image {
                    id: mask1
                    source: "thresholdmask.png"
                    width: image_body1.width
                    height: image_body1.height
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    visible: false
                }

                ThresholdMask {
                    id: thresholdmask_effect
                    anchors.fill: coffee1
                    source: coffee1
                    maskSource: mask1
                    spread: spread_slider.value
                    threshold: threshold_slider.value
                }
            }

            ColumnLayout{
                id: image_body2
                anchors.top: effects_pages.bottom
                anchors.horizontalCenter: effects_page_body.horizontalCenter
                visible: false

                Image {
                    id: coffee2
                    source: "coffee.jpg"
                    width: image_body2.width
                    height: image_body2.height
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    visible: false

                }

                Image {
                    id: mask2
                    source: "opacitymask.png"
                    width: image_body2.width
                    height: image_body2.height
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    visible: false
                }

                OpacityMask{
                    id: opacitymask_effect
                    maskSource: mask2
                    anchors.fill: coffee2
                    source: coffee2
                    invert: invert_effect.checked
                }
            }

            ColumnLayout{
                id: image_body3
                anchors.top: effects_pages.bottom
                anchors.horizontalCenter: effects_page_body.horizontalCenter
                visible: false

                Image {
                    id: coffee3
                    source: "coffee.jpg"
                    width: image_body3.width
                    height: image_body3.height
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                    visible: false
                }

                BrightnessContrast{
                    id: brightnesscontrast_effect
                    anchors.fill: coffee3
                    source: coffee3
                    brightness: brightness_slider.value
                    contrast: contrast_slider.value
                 }
            }

            RowLayout{
                anchors.top: image_body.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                ColumnLayout{
                    id: thresholdmask
                    visible: true

                    RowLayout{
                        Layout.fillWidth: true

                        Text {
                            text: "threshold"
                            color: "white"
                        }

                        Slider{
                            id: threshold_slider
                            Layout.fillWidth: true
                            Material.accent: "#3ecaff"
                            from: 0.0
                            to: 1.0
                            stepSize: 0.05
                        }

                        Text {
                            text: qsTr(threshold_slider.value.toFixed(2))
                            color: "white"
                        }
                    }

                    RowLayout{
                        Layout.fillWidth: true

                        Text {
                            text: "spread"
                            color: "white"
                        }

                        Slider{
                            id: spread_slider
                            Layout.fillWidth: true
                            Material.accent: "#3ecaff"
                            from: 0.0
                            to: 1.0
                            stepSize: 0.05
                        }

                        Text {
                            text: qsTr(spread_slider.value.toFixed(2))
                            color: "white"
                        }
                    }

                    Button {
                        id: dialog_btn1
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Сохранить изображение"
                        onClicked: saveFileDialog1.open()

                        FileDialog {
                            id: saveFileDialog1
                            title: "Save file"
                            nameFilters: ["Image files (*.jpg *.png)", "All files (*)"]
                            //fileMode: FileDialog.SaveFile
                            selectExisting: false

                            onAccepted:
                            {
                                var url = (saveFileDialog1.fileUrl.toString()+"").replace('file:///', '');
                                image_body1.grabToImage(function(result) {
                                    //console.log(saveFileDialog1.fileUrl.toString())
                                                           result.saveToFile(url);
                                                       });
                            }
                        }
                    }
                }

                ColumnLayout{
                    id: opacitymask
                    visible: false
                    anchors.horizontalCenter: parent.horizontalCenter

                    RowLayout{

                        Text {
                            text: "mask this image"
                            color: "white"
                        }

                        Switch {
                            id: invert_effect
                            checked: false
                            Material.accent: "#3ecaff"
                            anchors.bottomMargin: 100
                        }
                    }

                    RowLayout{
                    Button {
                        id: dialog_btn2
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Сохранить изображение"
                        onClicked: saveFileDialog2.open()

                        FileDialog {
                            id: saveFileDialog2
                            title: "Save file"
                            nameFilters: ["Image files (*.jpg *.png)", "All files (*)"]
                            //fileMode: FileDialog.SaveFile
                            selectExisting: false

                            onAccepted:
                            {
                                var url = (saveFileDialog2.fileUrl.toString()+"").replace('file:///', '');
                                image_body2.grabToImage(function(result) {
                                    //console.log(saveFileDialog1.fileUrl.toString())
                                                           result.saveToFile(url);
                                                       });
                            }
                        }
                    }
                    }
                }

                ColumnLayout{
                    id: brightnesscontrast
                    visible: false

                    RowLayout{
                        Layout.fillWidth: true

                        Text {
                            text: "brightness"
                            color: "white"
                        }

                        Slider{
                            id: brightness_slider
                            Layout.fillWidth: true
                            Material.accent: "#3ecaff"
                            from: -1.0
                            to: 1.0
                            stepSize: 0.05
                        }

                        Text {
                            text: qsTr(brightness_slider.value.toFixed(2))
                            color: "white"
                        }
                    }

                    RowLayout{
                        Layout.fillWidth: true

                        Text {
                            text: "contrast"
                            color: "white"
                        }

                        Slider{
                            id: contrast_slider
                            Layout.fillWidth: true
                            Material.accent: "#3ecaff"
                            from: -1.0
                            to: 1.0
                            stepSize: 0.05
                        }

                        Text {
                            text: qsTr(contrast_slider.value.toFixed(2))
                            color: "white"
                        }
                    }

                    Button {
                        id: dialog_btn3
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Сохранить изображение"
                        onClicked: saveFileDialog3.open()

                        FileDialog {
                            id: saveFileDialog3
                            title: "Save file"
                            nameFilters: ["Image files (*.jpg *.png)", "All files (*)"]
                            //fileMode: FileDialog.SaveFile
                            selectExisting: false

                            onAccepted:
                            {
                                var url = (saveFileDialog3.fileUrl.toString()+"").replace('file:///', '');
                                image_body3.grabToImage(function(result) {
                                    //console.log(saveFileDialog1.fileUrl.toString())
                                                           result.saveToFile(url);
                                                       });
                            }
                        }
                    }
                }
            }
        }
    }
}
