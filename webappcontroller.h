#ifndef WEBAPPCONTROLLER_H
#define WEBAPPCONTROLLER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QEventLoop>

class WebAppController : public QObject
{
    Q_OBJECT
public:
    explicit WebAppController(QObject *parent = nullptr); // конструктор класса

    QNetworkAccessManager * nam = nullptr; // поле nam класса

    ~WebAppController() { // деструктор класса
        delete nam;
    }

public slots:
    void getPageInfo(); // слот для составления http-запроса и отправки запроса к выбранному серверу
    void onNetworkValue(QNetworkReply *reply); // слот для приема возвращенной веб-сервером  информации и
                                               // поиска информации (в моем случае даты) из кода веб-страницы
protected:
    QObject * showInfo; // создание объекта QObject для нахождения необходимых компонентов в qml
};

#endif // WEBAPPCONTROLLER_H
