import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
import QtGraphicalEffects 1.0 // без него не будет тени
import QtMultimedia 5.14


ApplicationWindow {
    id: mainWindow
    width: 320
    height: 568
    visible: true
    title: qsTr("Программирование безопасных сетевых приложений")
    signal btnHttpRequest()

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        // Лабораторная работа №1
        Page1_GUI {
            id: page1_GUI
        }

        Page2_video_and_audio {
            id: page2_video_and_audio
        }

        Page3_graphic_effects {
            id: page3_graphic_effects
        }

        Page4_http_requests {
            id: page4_http_requests
        }

        Page {
            header: Label {
                text: "Тут будет лабораторная №4"
            }
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        Material.accent: "#3ecaff"

        TabButton {
            text: qsTr("Lab 1")
            Material.accent: "#3ecaff"
        }
        TabButton {
            text: qsTr("Lab 2")
            Material.accent: "#3ecaff"
        }
        TabButton {
            text: qsTr("Additional lab")
            Material.accent: "#3ecaff"
        }
        TabButton {
            text: qsTr("Lab 3")
            Material.accent: "#3ecaff"
        }
        TabButton {
            text: qsTr("Lab 4")
            Material.accent: "#3ecaff"
        }
    }
}
