import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
import QtGraphicalEffects 1.0 // без него не будет тени
import QtMultimedia 5.14


Page {
    id: page4_http_requests

    header: Rectangle {
        id: page_header
        color: "#020202"
        y: 0; height: 50

        Label {
            text: "Запросы к серверу"
            leftPadding: 10
            // для выравнивания текста по центру по левой стороне
            anchors.verticalCenter: page_header.verticalCenter
            anchors.left: page_header.left
        }

        Image {
            id: overflow_vertical_img
            source: "overflow_vertical.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: page_header.right
        }

        Image {
            id: edit_img
            source: "edit.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: overflow_vertical_img.left
            anchors.rightMargin: 10
        }

        Image {
            id: search_img
            source: "search.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: edit_img.left
            anchors.rightMargin: 10
        }

        Image {
            id: reload_img
            source: "page_reload.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: search_img.left
            anchors.rightMargin: 10
        }
    }

    ColumnLayout {
        anchors.top: page_header.bottom
        anchors.topMargin: 10
        anchors.fill: parent

        Button {
            id: btnRequest
            Layout.alignment: Qt.AlignCenter
            onClicked: mainWindow.btnHttpRequest();
            text: "Отправить"
        }

        RowLayout {
            id: radiobuttons
            anchors.top: btnRequest.bottom
            Layout.alignment: Qt.AlignCenter

            Label{
                text: qsTr("RichText")
            }

            RadioButton{
                id: richTextBtn
                checked: true
                onClicked: response.textFormat = Text.RichText
                Material.accent: "#3ecaff"
            }

            Label{
                text: qsTr("PlainText")
            }

            RadioButton{
                id: plainTextBtn
                onClicked: response.textFormat = Text.PlainText
                Material.accent: "#3ecaff"
            }
        }

        ColumnLayout{
            id: response_body
            anchors.top: radiobuttons.bottom

            Rectangle{
                color: "lavender"
                anchors.fill: response_body
            }

            ScrollView {
                Layout.fillWidth: true
                Layout.fillHeight: true
                clip: true

                Text{
                    id: response
                    anchors.margins: 15
                    anchors.fill: parent
                    objectName: "textArea"
                    textFormat: Text.RichText
                }
            }
        }

        ColumnLayout{
            anchors.top: response_body.bottom
            anchors.topMargin: 15
            Layout.alignment: Qt.AlignCenter

            Label {
                Layout.alignment: Qt.AlignCenter
                text: "Курс доллара"
            }

            RowLayout{
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignCenter
                anchors.leftMargin: 25
                anchors.rightMargin: 25

                TextField {
                    id: textField
                    objectName: "textField"
                    horizontalAlignment: Text.AlignHCenter
                    readOnly: true
                    Layout.alignment: Qt.AlignCenter
                    Material.accent: "blue"
                }
            }
        }
    }
}
