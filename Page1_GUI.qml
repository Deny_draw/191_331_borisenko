import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
import QtGraphicalEffects 1.0 // без него не будет тени


Page {
    id: page1_GUI

    // Оригинальный дизайн: https://sun9-35.userapi.com/impf/PxzmeOG0c4iSgFd74mnWjqtmably__rm4kc03g/R3BsSesZSPg.jpg?size=337x600&quality=96&sign=0d68622965c25f07cc9ddc10d960fa7e&type=album
    header: Rectangle {
        id: page_header
        color: "#020202"
        y: 0; height: 50

        Label {
            text: "Элементы GUI"
            leftPadding: 10
            // для выравнивания текста по центру по левой стороне
            anchors.verticalCenter: page_header.verticalCenter
            anchors.left: page_header.left
        }

        Image {
            id: overflow_vertical_img
            source: "overflow_vertical.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: page_header.right
        }

        Image {
            id: edit_img
            source: "edit.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: overflow_vertical_img.left
            anchors.rightMargin: 10
        }

        Image {
            id: search_img
            source: "search.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: edit_img.left
            anchors.rightMargin: 10
        }

        Image {
            id: reload_img
            source: "page_reload.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: search_img.left
            anchors.rightMargin: 10
        }
    }

    GridLayout {
        anchors.fill: parent
        rows: 5
        columns: 3
        anchors.leftMargin: 15
        anchors.rightMargin: 15

        // Мой вариант под номером 2
        // fio group
        Label {
            id: fio
            Layout.column: 0
            Layout.row: 0
            text: "Введите ФИО:"
        }

        TextArea {
            Layout.column: 1
            Layout.row: 0
            Layout.alignment: Qt.AlignLeft
            Layout.fillWidth: true
            Material.accent: "#3ecaff"
        }

        // gender group
        Label {
            id: gender
            Layout.column: 0
            Layout.row: 1
            text: "Выберите пол:"
        }

        Label {
            id: male
            Layout.column: 0
            Layout.row: 2
            text: "Мужской"
            anchors.top: gender.bottom
            anchors.topMargin: 15
        }

        RadioButton {
            Layout.column: 0
            Layout.row: 2
            anchors.left: male.right
            anchors.verticalCenter: male.verticalCenter
            Material.accent: "#3ecaff"
        }

        Label {
            id: female
            Layout.column: 0
            Layout.row: 3
            text: "Женский"
            anchors.top: male.bottom
            anchors.topMargin: 15
        }

        RadioButton {
            Layout.column: 0
            Layout.row: 3
            anchors.left: female.right
            anchors.verticalCenter: female.verticalCenter
            Material.accent: "#3ecaff"
        }

        // dial
        Dial {
            id: dial
            Layout.column: 0
            Layout.row: 3
            Material.accent: "#3ecaff"
            Layout.fillWidth: true
        }

        // range slider
        RangeSlider{
            Layout.column: 0
            Layout.row: 4
            Layout.columnSpan: 2
            Material.accent: "#3ecaff"
            Layout.fillWidth: true
        }

        // slider
        Rectangle {
            id: sound
            color: "white"
            Layout.column: 2
            Layout.row: 1
            Layout.rowSpan: 3
            width: 36
            height: 160
            radius: 20
            Layout.fillHeight: true

            Slider {
                orientation: Qt.Vertical
                anchors.horizontalCenter: sound.horizontalCenter
                anchors.verticalCenter: sound.verticalCenter
                height: sound.height - 30
                Material.accent: "blue"
            }
        }

        DropShadow {
            Layout.column: 2
            Layout.row: 1
            Layout.rowSpan: 3
            anchors.fill: sound
            horizontalOffset: 4
            verticalOffset: 3
            radius: 8.0
            samples: 17
            color: "#80000000"
            source: sound
        }

        // check group
        Label {
            id: check
            Layout.column: 0
            Layout.columnSpan: 2
            Layout.row: 5
            text: "Я прочитал и согласен со всем"
        }

        CheckBox {
            Layout.column: 1
            Layout.row: 5
            anchors.left: check.right
            anchors.verticalCenter: check.verticalCenter
            Material.accent: "#3ecaff"
        }
    }
}
