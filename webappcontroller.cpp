#include "webappcontroller.h"

WebAppController::WebAppController(QObject *parent) : showInfo(parent)
{
    nam = new QNetworkAccessManager(this); // создаем NetworkAccessManager
    connect(nam, &QNetworkAccessManager::finished, this, &WebAppController::onNetworkValue);
}

void WebAppController::getPageInfo(){
    QEventLoop evtLoop;
    connect(nam, &QNetworkAccessManager::finished,
            &evtLoop, &QEventLoop::quit);
    //QNetworkReply *reply = nam->get(QNetworkRequest(QUrl("https://yandex.ru/")));
    nam->get(QNetworkRequest(QUrl("https://yandex.ru/"))); // получаем ответ от https://yandex.ru/
    /*while(!reply->isFinished())
        qDebug() << "ожидание...";*/
    evtLoop.exec();

    //qDebug() << "...Ожидание сервера завершено, ответ: ";
    //qDebug() << reply->readAll();
}

void WebAppController::onNetworkValue(QNetworkReply *reply)
{ // парсинг сайта

    QString str = reply->readAll(); // записываем в str полученную страницу

    //qDebug() << "ываываэжЫЭВАЭФЖВЫЭАЖЭФЫЖВАЭЖФЭЫВЖАЭФЖЫВЭАЖФЭЫВЖ0" << str;

    QObject* textArea = showInfo->findChild<QObject*>("textArea"); // ищем элемент textAreaв qml, куда будем вписывать str
    QObject* textField = showInfo->findChild<QObject*>("textField"); // ищем элемент textField в qml, куда будем вписывать дату

    textArea->setProperty("text", str); // передаем в text textArea полученный ответ (str)
    int i = str.indexOf("inline-stocks__value_inner"); // ищем первое вхождение подстроки "inline-stocks__value_inner"
    textField->setProperty("text", str.mid(i + 28,5)); // получаем курс доллара путем пропуска и вывода нужных символов
}
