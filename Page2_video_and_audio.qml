import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12 // без него не будет работать механизм layout
import QtQuick.Controls.Material 2.3
import QtGraphicalEffects 1.0 // без него не будет тени
import QtMultimedia 5.14
import QtQuick.Dialogs 1.0


Page {
    id: page2_video_and_audio
    // Оригинальный дизайн: https://sun9-35.userapi.com/impf/PxzmeOG0c4iSgFd74mnWjqtmably__rm4kc03g/R3BsSesZSPg.jpg?size=337x600&quality=96&sign=0d68622965c25f07cc9ddc10d960fa7e&type=album
    header: Rectangle {
        id: page_header
        color: "#020202"
        y: 0; height: 50

        Label {
            text: "Работа с фото и видео"
            leftPadding: 10
            // для выравнивания текста по центру по левой стороне
            anchors.verticalCenter: page_header.verticalCenter
            anchors.left: page_header.left
        }

        Image {
            id: overflow_vertical_img
            source: "overflow_vertical.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: page_header.right
        }

        Image {
            id: edit_img
            source: "edit.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: overflow_vertical_img.left
            anchors.rightMargin: 10
        }

        Image {
            id: search_img
            source: "search.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: edit_img.left
            anchors.rightMargin: 10
        }

        Image {
            id: reload_img
            source: "page_reload.png"
            width: 25
            height: 25
            anchors.verticalCenter: page_header.verticalCenter
            anchors.right: search_img.left
            anchors.rightMargin: 10
        }
    }

    ColumnLayout{
        anchors.fill: parent

        RowLayout {
            id: change_page
            Layout.alignment: Qt.AlignCenter

            RadioButton {
                id: video
                text: qsTr("Видео")
                Material.accent: "#3ecaff"
                checked: true
            }

            RadioButton {
                id: camera
                text: qsTr("Камера")
                Material.accent: "#3ecaff"
            }
        }

        ColumnLayout{
            id: body

            Rectangle{
                color: "black"
                anchors.fill: body
                id: video_page

                Timer {
                    id: video_timer
                    interval: 5000
                    running: true
                    repeat: true
                    onTriggered: video_manipulators.visible = false
                }

                Item {
                    id: video_output
                    height: video_page.height
                    width: video_page.width

                    MediaPlayer {
                        id: robot_video
                        source: "robot.mp4"
                        loops: MediaPlayer.Infinite
                    }

                    VideoOutput {
                        anchors.fill: parent
                        source: robot_video

                        MouseArea {
                           id: playArea
                           anchors.fill: parent
                           onPressed: {
                               robot_video.playbackState == MediaPlayer.PlayingState ? robot_video.pause() : robot_video.play()
                               video_manipulators.visible = true
                               video_timer.restart()
                           }
                        }
                    }

                    visible: {
                        if(video.checked == true){
                            video_manipulators.visible = true
                            return true
                        }
                        else{
                            robot_video.pause()
                            video_manipulators.visible = false
                            return false
                        }
                    }
                }

                Item{
                    id: camera_page
                    anchors.fill: parent
                    anchors.verticalCenter: parent.verticalCenter

                    Camera{
                        id: photo_video
                    }

                    VideoOutput {
                        source: photo_video
                        anchors.fill: parent
                        autoOrientation: true
                    }

                    visible: {
                        if(camera.checked == true)
                            return true
                        else
                            return false
                    }
                }
            }
        }

        ColumnLayout{
            id: video_manipulators

            RowLayout{
                Slider{
                    id: video_duration
                    Layout.fillWidth: true
                    to: robot_video.duration
                    value: robot_video.position
                    onPressedChanged: {
                        robot_video.seek(video_duration.value)
                    }
                    Material.accent: "#3ecaff"
                }

                Text{
                    id: video_time
                    anchors.right: parent.right
                    anchors.rightMargin: 5
                    font.pointSize: 10
                    color: "white"
                    text: Qt.formatTime(new Date(robot_video.position), "mm:ss")
                }
            }

            RowLayout{
                id: video_buttons
                Layout.alignment: Qt.AlignCenter
                anchors.bottomMargin: 15

                Button{
                    id: start_stop
                    icon.source: {
                        if (MediaPlayer.PlayingState == robot_video.playbackState)
                            return "pause.png"
                        else
                            return "play.png"
                    }
                    onClicked: {
                        robot_video.playbackState == MediaPlayer.PlayingState ? robot_video.pause() : robot_video.play()
                        video_timer.restart()
                    }
                }
            }
            visible: {
                robot_video.pause()
                if(video.checked == true)
                    return true

                else{
                    return false
                }
            }
        }

        ColumnLayout{
            Layout.alignment: Qt.AlignCenter
            RowLayout{
                Button{
                    id: take_photo
                    icon.source: "photocamera.png"
                    onClicked: photo_video.imageCapture.capture();
                }

                Button{
                    id: start_stop_record_video
                    icon.source: {
                        if (photo_video.videoRecorder.recorderStatus == CameraRecorder.RecordingStatus)
                            return "pause.png"
                        else
                            return "play.png"
                    }
                    onClicked: {
                        if (photo_video.videoRecorder.recorderState == CameraRecorder.StoppedState){
                            photo_video.videoRecorder.outputLocation = "C:\\Qt\\Smth\\191_331_Borisenko\\video";
                            photo_video.videoRecorder.record() // начать запись
                             //console.log(photo_video.videoRecorder.recorderStatus)
                        }
                        else
                            photo_video.videoRecorder.stop() // остановить

                    }
                }
            }

            visible: {
                if(camera.checked == true)
                    return true
                else
                    return false
            }
        }
    }
}
